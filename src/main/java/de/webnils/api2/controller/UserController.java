package de.webnils.api2.controller;

import de.webnils.api2.model.User;
import de.webnils.api2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("")
    public String test(){
        return "Hallo Welt";
    }

    @PostMapping("/add")
    public String add(@RequestBody User user){
        userService.saveUser(user);
        return "New User is added";
    }
    @GetMapping("/getAll")
    public List<User> getAllUsers(){
        System.out.println("Hallo Welt");
        return userService.getAllUsers();
    }
    @GetMapping("/getId")
    public List<User> getAllUsersById(@RequestParam("id") String data){
            var id = Integer.parseInt(data);
        return userService.getAllUsersById(id);
    }
}
