package de.webnils.api2.service;

import de.webnils.api2.model.User;

import java.util.List;

public interface UserService {
    public User saveUser(User user);
    public List<User> getAllUsers();
    public List<User> getAllUsersById(int id);
}
