FROM openjdk:17
ADD /target/api2-0.0.1-SNAPSHOT.jar api2-0.0.1-SNAPSHOT.jar
ENV PORT=8080
ENTRYPOINT ["java", "-jar","api2-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080